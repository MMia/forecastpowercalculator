package services;

public class IncorrectHeaderDateException extends RuntimeException{
    IncorrectHeaderDateException(Exception e) {
        super(e);
    }
}
