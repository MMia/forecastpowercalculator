package services;

import model.DayCapacityForecast;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class DataProvider {
    private static final char CHAR_DATE_SPLITTER = 160;
    private static final String DATE_SPLITTER = String.valueOf(CHAR_DATE_SPLITTER);
    private static final String HEADER_ROWS_SELECTOR ="th";
    private static final String DATA_ROWS_SELECTOR ="tr";
    private static final String FIRST_DATA_ROW_SELECTOR ="tr:eq(2)";
    private static final String COLUMN_CELLS_SELECTOR ="td";
    private static final String ACTUAL_CELL_SELECTOR ="txtrVERIF";
    private static final String FORECAST_CELL_SELECTOR ="txtrPREV";
    private static final String FORECAST_CELL_SECOND_SELECTOR ="txtPREV";

    private List<DayCapacityForecast> daysAndValuesFromCapacityForecastTable = Collections.emptyList();

    public List<DayCapacityForecast> getDaysAndValuesFromCapacityForecastTable() {
        try {
            addCapacityValuesForDaysFromTable();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return daysAndValuesFromCapacityForecastTable;
    }

    private void addCapacityValuesForDaysFromTable() throws IOException {
        Element table = getTableFromHtml();

        Elements headerRows = table.select(HEADER_ROWS_SELECTOR);
        createListOfDaysCapacityForecastAndAddDaysFromTable(headerRows);

        Elements dataRows = table.select(DATA_ROWS_SELECTOR);
        int numberOfColumnsInTable = getNumberOfColumnsInTable(dataRows);

        for (int i = 1; i <= numberOfColumnsInTable-1; i++) {
            Elements column = dataRows.select("td:eq(" + i + ")");
            DayCapacityForecast dayInColumn = daysAndValuesFromCapacityForecastTable.get(i-1);
            for (Element cell : column) {
                addCapacityToDayByType(dayInColumn, cell);
            }
        }
    }

    private Element getTableFromHtml() {
        ForecastPowerTableProvider tablePovider = new ForecastPowerTableProvider();
        return tablePovider.getForecastPowerTable();
    }

    private void createListOfDaysCapacityForecastAndAddDaysFromTable(Elements days) {
        daysAndValuesFromCapacityForecastTable = new LinkedList<>();
        for (Element cell : days) {
            String date = cell.ownText();

            if (date.contains(DATE_SPLITTER)){
                LocalDate headerDate = formatDate(date);
                DayCapacityForecast headerDay = new DayCapacityForecast(headerDate);
                daysAndValuesFromCapacityForecastTable.add(headerDay);
            }
        }
    }

    private int getNumberOfColumnsInTable(Elements dataRows) {
        Elements firstDataRow = dataRows.select(FIRST_DATA_ROW_SELECTOR);

        return firstDataRow.select(COLUMN_CELLS_SELECTOR).size();
    }

    private void addCapacityToDayByType(DayCapacityForecast dayInColumn, Element cell) {
        if(cell.className().equals(ACTUAL_CELL_SELECTOR)){
            dayInColumn.addActualCapacity(Integer.valueOf(cell.ownText()));
        }
        if(cell.className().equals(FORECAST_CELL_SELECTOR) ||
                cell.className().equals(FORECAST_CELL_SECOND_SELECTOR)){
            dayInColumn.addForecastCapacity(Integer.valueOf(cell.ownText()));
        }
    }

    private LocalDate formatDate(String date) {
        try {
            String dayHeaderElements[] = date.split(DATE_SPLITTER);
            date = dayHeaderElements[0]+dayHeaderElements[1]+getActualYear();
            return LocalDate.parse(date, DateTimeFormatter.ofPattern("ddMMMyyyy", Locale.US));
        } catch (DateTimeParseException e) {
            throw new IncorrectHeaderDateException(e);
        }
    }

    private String getActualYear() {
        int year = LocalDate.now().getYear();

        return String.valueOf(year);
    }
}
