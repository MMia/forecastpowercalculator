package services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;

public class ForecastPowerTableProvider {
    private static final String HTML_TABLE_SELECTOR = "table.gridALL";
    private static final String LOCAL_HTML_RESOURCE_PATH = "SIMEE-Daily.htm";
    //private static final String URL = "http://www.mercado.ren.pt/EN/Electr/MarketInfo/Interconnections/CapForecast/Pages/Daily.aspx";

    public Element getForecastPowerTable() {
        Document document = getDocumentFromLocalHtmlResource();

        return document.select(HTML_TABLE_SELECTOR).first();
    }

    private Document getDocumentFromLocalHtmlResource() {
        File inputFile = new File(getClass().getClassLoader().getResource(LOCAL_HTML_RESOURCE_PATH).getFile());
        Document document = null;
        try {
            document = Jsoup.parse(inputFile, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return document;
    }

     /*  private Document getDocumentFromInternetHtmlResource() throws IOException {
        Connection connection = Jsoup.connect(URL);
        Document document = connection.get();
        return document;
    }
    */
}
