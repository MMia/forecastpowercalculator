import services.DataProvider;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        DataProvider dt = new DataProvider();
        dt.getDaysAndValuesFromCapacityForecastTable()
                .forEach(dayCapacityForecast -> System.out.println(dayCapacityForecast.toString()));
    }
}
