package model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

public class DayCapacityForecast {

    private LocalDate date;
    private List<Integer> actual = new LinkedList<>();
    private List<Integer> forecast = new LinkedList<>();

    public DayCapacityForecast(LocalDate date) {
        this.date = date;
    }

    public void addActualCapacity(int capacity) {
        actual.add(capacity);
    }

    public void addForecastCapacity(int capacity) {
        forecast.add(capacity);
    }

    public int getActualSum() {
        return actual.stream().reduce(0, Integer::sum);
    }

    public int getForecastSum() {
        return forecast.stream().reduce(0, Integer::sum);
    }

    @Override
    public String toString() {
        String formattedDate = DateTimeFormatter.ISO_DATE.format(this.date);
        String actual = String.valueOf(getActualSum());
        String forecast = String.valueOf(getForecastSum());
        return buildOutput(formattedDate, actual, forecast);
    }

    private String buildOutput(String formattedDate, String actual, String forecast) {
        String actualOutput = "Actual: " + formattedDate + ": <" + actual + ">";
        String forecastOutput = "Forecast: " + formattedDate + ": <" + forecast + ">";
        return actualOutput + "\n" + forecastOutput;
    }
}
